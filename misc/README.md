bacontools/misc - chaos reigns
==============================
| Tool              | Maturity | Description                                             | Language    |
|-------------------|----------|---------------------------------------------------------|-------------|
| EncodingConverter | Untested | Convert between different encodings                     | C#          |
| apdiff            | Hack     | Print differing parts of similar file paths             | Python      |
| baconplaylist     | Hack     | Query a list in a specific format                       | POSIX shell |
| bananaglee        | Hack     | Generate a USA federal agency-like project identifier   | Haskell     |
| bitcount          | Hack     | Tally individual bits in stdin bytes                    | C           |
| bitdiff           | Hack     | Detect different bytes in mostly similar files          | C           |
| byteat            | Hack     | Print value of byte at index                            | C           |
| clone-github-user | Hack     | Clone all repositories of a single Github user          | POSIX shell |
| corrupt           | Untested | Flip/remove random bits/bytes                           | C           |
| git-ls            | Untested | Github-like human-readable Git repo directory listing   | Python      |
| git-repo-list     | Hack     | Clone or pull all repos from a remote list              | POSIX shell |
| git-stat-atr      | Hack     | Sort output of `git diff --stat` by added/total ratio   | POSIX shell |
| maybe             | Hack     | Prints yes and no randomly interleaved                  | C           |
| pip-upgrade-all   | Hack     | Upgrade all local PIP packages                          | Python      |
| stopwatch         | Hack     | Count elapsed time                                      | POSIX shell |
| tasktags          | Untested | Search for tags like TODO                               | POSIX shell |
| termdraw          | Moving   | Print ASCII-art graphs                                  | Python      |
| update-all        | Hack     | Update all Git repositories in level 1 subdirectories   | POSIX shell |
