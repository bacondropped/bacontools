clone-github-user - ceaseless discharge of samefaced persons
============================================================
`clone-github-user` accepts a Github user or organization name and clones all
their public repositories to the working directory.
