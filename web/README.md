bacontools/web - welcome to the lair of Ungoliant
=================================================
| Tool      | Maturity | Description                            | Language    |
|-----------|----------|----------------------------------------|-------------|
| curl-tt   | Hack     | Test server response time              | POSIX shell |
| httpdf    | Hack     | Listen to a port and return free space | Go          |
| myzuka-dl | Moving   | Download audio tracks from myzuka.fm   | Ruby        |
| imgur-dl  | Moving   | Download Imgur albums                  | Ruby        |
| respcode  | Hack     | Return HTTP response code              | POSIX shell |
| wget-page | Hack     | Download a web page                    | POSIX shell |
