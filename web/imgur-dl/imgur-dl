#!/usr/bin/env ruby
#
# TODO: Massive code refactoring
# TODO: Download groups of posts: user's submissions, faves, fp, by search, etc
# TODO: Text search
# TODO: Optional comment scraping for URLs
# TODO: Optional post metadata export to JSON
# FIXME: Some images yield a redirect loop; investigate

require 'rubygems'
require 'capybara'
require 'capybara/poltergeist'
require 'highline/import'
require 'net/http'
require 'open-uri'
require 'open_uri_redirections'
require 'optparse'
require 'ostruct'
require 'uri'
require 'fileutils'

TIMEOUT = 60
STDNULL = File.open(File::NULL, 'w')

module Imgur
  class Downloader
    def download(url, opts)
      session = Capybara::Session.new(:poltergeist)
      session.visit(url)
      images = []

      session.visit(session.current_url.gsub(/gallery/, "a").gsub(/(?<!\/layout\/grid)$/, '/layout/grid'))

      expected_imgs = Integer session.find(:xpath, "//h2[contains(text(),'image')]").text[/(\d+) image(s)?$/, 1]
      STDERR.puts("Expecting #{expected_imgs} images")

      loop do
        STDERR.puts("Scrolling down")
        session.execute_script('window.scrollBy(0,100000)')
        sleep 2
        images = session.all(:xpath, "//div[@class='posts']/div[.//img]")
        STDERR.puts("Found #{images.size} images out of expected #{expected_imgs}")
        break unless images.size < expected_imgs
      end

      post_id = session.current_url[/(?<=\/a\/)([A-Za-z0-9]*)/, 1]
      post_title = session.find(:xpath, "//title").text.gsub("- Album on Imgur", "").strip

      subdir = "#{post_id} #{("- " + post_title) unless post_title =~ /Imgur: The most awesome images on the Internet/ or post_title.empty?}".strip
      path = opts.path
      path = subdir if path == "."
      path = path.tr("/\000", "")

      FileUtils::mkdir_p(path)

      images.each_with_index do |img, index|
        image_url  = img.find(:xpath, ".//img")[:src].gsub("b.", ".")
        request    = open(image_url)
        basename   = File.basename(URI.parse(image_url).path)
        basename   = "#{index+1} - #{basename}".tr("/\000", "")
        final_path = File.join(path, basename)
        puts("#{final_path}")
        IO.copy_stream(request, final_path)
      end
    end
  end
end

$terminal = HighLine.new($stdin, $stderr)

cli_options  = OpenStruct.new

OptionParser.new do |option|
  option.on('-d PATH', '--dir PATH') {|o| cli_options.path = o}
end.parse!

cli_options.path = '.' unless cli_options.path

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, {js_errors: false, timeout: TIMEOUT, phantomjs_logger: STDNULL})
end

Capybara.run_server              = false
Capybara.current_driver          = :poltergeist
Capybara.ignore_hidden_elements  = false

if ARGV.empty? then
  Imgur::Downloader.new.download(ask("Album url: "), cli_options)
end

ARGV.each do |arg|
  Imgur::Downloader.new.download(arg, cli_options)
end
