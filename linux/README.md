bacontools/linux - gaze into the eyes of the penguin
====================================================
| Tool         | Maturity | Description                                    | Language    |
|--------------|----------|------------------------------------------------|-------------|
| checkreboot  | Hack     | Print whether reboot is required               | POSIX shell |
| cptemp       | Hack     | Copy file/directory to /tmp                    | POSIX shell |
| devbup       | Moving   | Backup, archive, and encrypt files and devices | Bash        |
| devrestore   | Hack     | Restore devbup archives                        | Bash        |
| du1          | Hack     | Print sizes of top level directories           | POSIX shell |
| netinfo      | Untested | Print current WAN IP and nmcli connection name | POSIX shell |
| single-urxvt | Hack     | Launch a singleton urxvt instance              | POSIX shell |
| userls       | Hack     | Print all users on the system                  | POSIX shell |
